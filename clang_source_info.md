Base revision: [14f0776550b5a49e1c42f49a00213f7f3fa047bf](https://github.com/llvm/llvm-project/commits/14f0776550b5a49e1c42f49a00213f7f3fa047bf)

- [Add-stubs-and-headers-for-nl_types-APIs.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/Add-stubs-and-headers-for-nl_types-APIs.patch)
- [Ensure-that-we-use-our-toolchain-s-lipo-and-not-the-.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/Ensure-that-we-use-our-toolchain-s-lipo-and-not-the-.patch)
- [BOLT-Increase-max-allocation-size-to-allow-BOLTing-clang-and-rustc.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/BOLT-Increase-max-allocation-size-to-allow-BOLTing-clang-and-rustc.patch)
- [Revert-Enable-IAS-In-Backend-v2.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/Revert-Enable-IAS-In-Backend-v2.patch)
- [Disable-vfork-fork-events.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/Disable-vfork-fork-events.patch)
- [Revert-Driver-Allow-target-override-containing-.-in-executable-name.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/Revert-Driver-Allow-target-override-containing-.-in-executable-name.patch)
- [Wasm-omit-64-bit-function-pointer-cast.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/Wasm-omit-64-bit-function-pointer-cast.patch)
- [compiler-rt-Allow-finding-LLVMConfig-if-CMAKE_FIND_ROOT_PATH_MODE_PACKAGE-is-set-to-ONLY.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/compiler-rt-Allow-finding-LLVMConfig-if-CMAKE_FIND_ROOT_PATH_MODE_PACKAGE-is-set-to-ONLY.patch)
- [enable-libcxx-testing-using-adb.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/enable-libcxx-testing-using-adb.patch)
- [move-cxa-demangle-into-libcxxdemangle.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/move-cxa-demangle-into-libcxxdemangle.patch)
- [avoid-triggering-fdsan-in-filebuf-test.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/avoid-triggering-fdsan-in-filebuf-test.patch)
- [hide-locale-lit-features-for-bionic.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/hide-locale-lit-features-for-bionic.patch)
- [bionic-includes-plus-sign-for-nan.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/bionic-includes-plus-sign-for-nan.patch)
- [disable-symlink-resolve-test-on-android.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/disable-symlink-resolve-test-on-android.patch)
- [avoid-fifo-socket-hardlink-in-libcxx-tests.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/avoid-fifo-socket-hardlink-in-libcxx-tests.patch)
- [0001-Revert-BOLT-CMake-Use-correct-output-paths-and-passt.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/0001-Revert-BOLT-CMake-Use-correct-output-paths-and-passt.patch)
- [0001-Fix-interceptor-logic.patch](https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/master/patches/0001-Fix-interceptor-logic.patch)